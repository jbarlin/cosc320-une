
/**
 * hideOptionGroup: Hides and disables an option group
 */
$.fn.hideOptionGroup = function () {
    $(this).hide();
    $(this).children().each(function () {
        $(this).attr("disabled", "disabled").removeAttr("selected");
    });
    $(this).appendTo($(this).parent());

}

/**
 * showOptionGroup: Shows and enables an option group
 */
$.fn.showOptionGroup = function () {
    $(this).show();
    $(this).children().each(function () {
        $(this).removeAttr("disabled");
    });
    $(this).prependTo($(this).parent());
    $(this).parent().animate({ scrollTop: 0 }, 0);
}

/**
 * bodyParse: Replaces line breaks with paragraph tags
 * 
 * @param {*} inText HTML to parse
 */
function bodyParse(inText) {
    return inText.replace(/(?:\r\n|\r|\n)/g, '</p><p>');
}

/**
 * buildHTML: Builds the output HTML code based on data
 * entered in to template
 * 
 */
function buildHTML() {
    let outputhtml = "";

    // Build heading
    if ($("#jobtitle").val().length > 0) {
        outputhtml += "<h1>" + $("#jobtitle").val() + "</h1>";
    }
    if ($("#jobarea").val().length > 0) {
        outputhtml += "<h2>" + $("#jobarea").val() + "</h2>";
    }

    // Build header unordered list
    outputhtml += "<section aria-labelledby=\"une-hr-posnov-header\">";
    outputhtml += "<h3 id=\"une-hr-posnov-header\">Position overview</h3>";
    outputhtml += "<ul>";

    // Role type
    if ($("#roletype").val().length > 0) {
        outputhtml += "<li>" + $("#roletype").val() + "</li>";
    }

    // Position type
    outputhtml += "<li>"
    if ($('#posntype-dr4').val() != '') {
        outputhtml += $('#posntype-dr4').val() + " ";
    }
    outputhtml += $('#posntype-dr1').val() + ", " + $('#posntype-dr2').val();
    if ($('#posntype-dr3').val() != '') {
        outputhtml += " " + $('#posntype-dr3').val()
    }
    outputhtml += "</li>"

    // Superannuation
    if ($("#superpercentage").val().length > 0) {
        outputhtml += "<li>Plus " + $("#superpercentage").val() + "% employer superannuation";
        if ($('#posntype-dr1').val() == 'Continuing' || $('#posntype-dr1').val() == 'continuing') {
            outputhtml += ". Salary packaging options are available";
        }
        outputhtml += "</li>";
    }

    // Salary range
    if ($("#salarylow").val().length > 0 || $("#salaryhigh").val().length > 0) {
        if ($("#salarylow").val() == $("#salaryhigh").val() || $("#salaryhigh").val() == "") {
            outputhtml += "<li>$" + $("#salarylow").val();
        } else {
            outputhtml += "<li>$" + $("#salarylow").val() + " - $" + $("#salaryhigh").val();
        }
        outputhtml += $("#salarylabel").val();
        if ($("#posntier").val().length > 0) {
            outputhtml += " (" + $("#posntier").val() + ")";
        }
        outputhtml += "</li>";
    } else {
        if ($("#posntier").val().length > 0) {
            outputhtml += "<li>" + $("#posntier").val() + "</li>";
        }
    }

    // Relocation
    if ($("#relocation").val().length > 0) {
        outputhtml += "<li>" + $("#relocation").val() + "</li>";
    }

    // Visa requirements
    if ($("#visa").val().length > 0) {
        outputhtml += "<li>" + $("#visa").val() + "</li>";
    }
    outputhtml += "</ul>";
    outputhtml += "</section>";

    // About the role
    if ($("#abouttherole-header").val().length > 0 && $("#abouttherole-body").val().length > 0) {
        outputhtml += "<section aria-labelledby=\"une-hr-abtrlhd\"><h3 id=\"une-hr-abtrlhd\">" + $("#abouttherole-header").val() + "</h3>";
        outputhtml += "<p>" + bodyParse($("#abouttherole-body").val()) + "</p></section>";
    }

    // Skills & experience
    if ($("#skillsandexperience-header").val().length > 0 && $("#skillsandexperience-body").val().length > 0) {
        outputhtml += "<section aria-labelledby=\"une-hr-skillshd\"><h3 id=\"une-hr-skillshd\">" + $("#skillsandexperience-header").val() + "</section>";
        outputhtml += "<p>" + bodyParse($("#skillsandexperience-body").val()) + "</p></section>";
    }

    // Additional information
    if ($("#contactname").val().length > 0 || $("#contactposition").val().length > 0 || $("#contactnumber").val().length > 0 || $("#contactemail").val().length > 0) {
        outputhtml += "<section aria-labelledby=\"une-hr-additinfohd\"><h3 id=\"une-hr-additinfohd\">Additional information</h3><p>"
        outputhtml += "To discuss this opportunity, please contact ";
        if ($("#contactname").val().length > 0) {
            outputhtml += $("#contactname").val() + ", ";
        }
        if ($("#contactposition").val().length > 0) {
            outputhtml += $("#contactposition").val() + ",";
        }
        if ($("#contactnumber").val().length > 0) {
            outputhtml += " by phone: " + $("#contactnumber").val() + " ";
        }
        if ($("#contactemail").val().length > 0) {
            if ($("#contactnumber").val().length > 0) {
                outputhtml += " or "
            }
            outputhtml += " by email: <a href='mailto:" + $("#contactemail").val() + "'>" + $("#contactemail").val() + "</a>";
        }
        outputhtml += "</p></section>";
    }

    // About Armidale
    if ($("#aboutarmidale-header").val().length > 0 && $("#aboutarmidale-body").val().length > 0) {
        outputhtml += "<section aria-labelledby=\"une-hr-abtarmhd\"><h3 id=\"une-hr-abtarmhd\">" + $("#aboutarmidale-header").val() + "</h3>";
        outputhtml += "<p>" + bodyParse($("#aboutarmidale-body").val()) + "</p></section>";
    }

    // Close date
    outputhtml += "<section aria-labelledby=\"une-hr-furinfhd\"><h3 id=\"une-hr-furinfhd\">Further information</h3>";
    if ($("#closedate").val().length > 0) {
        var d = new Date($("#closedate").val());
        outputhtml += "<p>" + "<strong>Cosing Date: </strong>: " + (d.getDay()) + "/" + (d.getMonth() + 1) + "/" + (d.getFullYear()) + "</p>";
    }

    // Reference Number
    if ($("#referencenumber").val().length > 0) {
        outputhtml += "<p>" + "<strong>Reference No: </strong>: " + $("#referencenumber").val() + "</p>";
    }

    // Footer
    outputhtml += "<strong>To demonstrate your suitability for this role, please respond to the selection criteria, contained in";
    outputhtml += " the position statement, in your application.</strong> <br />";
    outputhtml += "<br />";
    outputhtml += "<a href='http://www.une.edu.au/jobs-at-une' target='_blank'>www.une.edu.au/jobs-at-une</a>";
    outputhtml += "<br /><br />";
    outputhtml += "<em>Equity principles underpin all UNE policies and procedures. As a University committed to engaging a ";
    outputhtml += "diverse workforce, UNE encourages Aboriginal and Torres Strait Islander people to apply.</em>";
    outputhtml += "</section>";
    return outputhtml;
}

/**
 * copyToClipboard: Copies string to user's clipboard
 * 
 * @param {*} str String to copy to clipboard
 */
function copyToClipboard(str) {
    // Create new element
    var el = document.createElement('textarea');
    // Set value (string to be copied)
    el.value = str;
    // Set non-editable to avoid focus and move outside of view
    el.setAttribute('readonly', '');
    el.style = { position: 'absolute', left: '-9999px' };
    document.body.appendChild(el);
    // Select text inside element
    el.select();
    // Copy text to clipboard
    document.execCommand('copy');
    // Remove temporary element
    document.body.removeChild(el);
    alert('Successfully copied to advert HTML code to your clipboard :)');
}

/**
 * jQuery document ready function
 */
$(document).ready(function () {
  
    $("#continuing").show();
    $("#casual").hide();

    // Build position type continuing or contract drop down
    $('#posntype-dr1').on('change', function () {
        if ($(this).val() == 'contract') {
            $('#posntype-dr4').attr('disabled', false);
            $('#posntype-dr4').show();
        } else {
            $('#posntype-dr4').attr('disabled', 'disabled');
            $('#posntype-dr4-naop').attr('selected', true);
            $('#posntype-dr4-naop').attr('selected', 'selected');
            $('#posntype-dr4').val('');
            $('#posntype-dr4').hide();
        }
    });

    // Build position type full-time and part-time drop down
    $('#posntype-dr2').on('change', function () {
        if ($(this).val() == 'part time') {
            $('#posntype-dr3').attr('disabled', false);
            $('#posntype-dr3').show();
        } else {
            $('#posntype-dr3').attr('disabled', 'disabled');
            $('#posntype-dr3-naop').attr('selected', true);
            $('#posntype-dr3-naop').attr('selected', 'selected');
            $('#posntype-dr3').val('');
            $('#posntype-dr3').hide();
        }
    });

    // Hide other position type drop downs until one dr1 and dr2 are selected
    $('#posntype-dr3').hide();
    $('#posntype-dr4').hide();

    // Build work days per fortnight drop down
    for (i = 1; i <= 9; i++) {
        var $op = $('<option>');
        $op.text(i + ' days per fortnight');
        $op.val(i + ' days per fortnight');
        $('#posntype-dr3-days').append($op);
    }

    // Build months contract drop down
    for (i = 3; i <= 24; i++) {
        var $op = $('<option>');
        $op.text(i + ' months');
        $op.val(i + ' month');
        $('#posntype-dr4-months').append($op);
    }

    // Update code button in the center of form
    $('#update-code').click(function () {
        var $article = $('<article>');
        $article.html(buildHTML());
        $article.css('border', '1px solid black');
        $('#output-div').html('<h2 id="output-header">Advert preview:</h2>' + $article[0].outerHTML);
    });

    // Copy code to output section
    $('#copy-code').click(function () {
        var $article = $('<textarea>');
        $article.text(buildHTML());
        $article.attr('id', 'output-pre');
        $('#output-div').html('<h2 id="output-header">Ready-to-go HTML:</h2>' + $article[0].outerHTML);
    });
});