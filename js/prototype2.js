/**
 *  This function changes the b and i tags to strong and em tags respectively
 * 
 * @param {string} inputhtml Input HTML to be parsed
 */
function clean_up_html_tags(inputhtml) {
    return inputhtml
        .replace("<b>", "<strong>")
        .replace("</b>", "</strong>")
        .replace("<i>", "<em>")
        .replace("</i>", "</em>")
        .replace("</font>", "</span>")
        .replace("<font color=\"", "<span style=\"color: ");
}

/**
 * jQuery document ready function
 */
$(document).ready(function () {
    // Set default paragraph separator to p tag instead of br
    document.execCommand("defaultParagraphSeparator", false, "p");

    // Clear out values to start with
    $('#input-textarea').text('');
    $('#input-textarea').val('');
    $('#input-textarea').on('input propertychange', function () {

    });

    // Update code button in the center of form
    $('#update-code').click(function () {
        let outputprehtml = $('#input-textarea').html();
        $('#output-pre').text(clean_up_html_tags(outputprehtml));

    });

    // Colour picker needs to be set twice in case they don't change the colour
    $("#color-picker")
        .click(function () {
            document.execCommand('foreColor', false, $("#color-picker").val());

            return true;
        })
        .change(function () {
            document.execCommand('foreColor', false, $("#color-picker").val());

            return true;
        });

    // Create list of buttons used to style the input like bold, italic etc
    var buttonList = [{
            // Bold button
            'type': 'icon',
            'icon': 'bold',
            'name': 'Bold',
            'id': 'bold',
            'title': 'Bold',
            'onclick': function () {
                document.execCommand('bold', false, '');
                return true;
            }
        },
        {
            // Italic button
            'type': 'icon',
            'icon': 'italic',
            'name': 'Italic',
            'id': 'italic',
            'title': 'Italic',
            'onclick': function () {
                document.execCommand('italic', false, '');
                return true;
            }
        },
        {
            // Underline button
            'type': 'icon',
            'icon': 'underline',
            'name': 'underline',
            'id': 'underline',
            'title': 'Bold',
            'onclick': function () {
                document.execCommand('underline', false, '');
                return true;
            }
        },
        {
            // Align left button
            'type': 'icon',
            'icon': 'align-left',
            'name': 'align-left',
            'id': 'align-left',
            'title': 'Left justify',
            'onclick': function () {
                document.execCommand('justifyLeft', false, '');
                return true;
            }
        },
        {
            // Align center button
            'type': 'icon',
            'icon': 'align-center',
            'name': 'align-center',
            'id': 'align-center',
            'title': 'Center justify',
            'onclick': function () {
                document.execCommand('justifyCenter', false, '');
                return true;
            }
        },
        {
            // Align right button
            'type': 'icon',
            'icon': 'align-right',
            'name': 'align-right',
            'id': 'align-right',
            'title': 'Right justify',
            'onclick': function () {
                document.execCommand('justifyRight', false, '');
                return true;
            }
        },
        {
            // Paragraph button
            'type': 'icon',
            'icon': 'paragraph',
            'name': 'paragraph',
            'id': 'paragraph',
            'title': 'Paragraph',
            'onclick': function () {
                document.execCommand('insertParagraph', false, '');
                return true;
            }
        },
        {
            // Unordered list button
            'type': 'icon',
            'icon': 'list-ul',
            'name': 'list-ul',
            'id': 'list-ul',
            'title': 'Unordered list',
            'onclick': function () {
                document.execCommand('insertUnorderedList', false, '');
                return true;
            }
        },
        {
            // Ordered list button
            'type': 'icon',
            'icon': 'list-ol',
            'name': 'list-ol',
            'id': 'list-ol',
            'title': 'Ordered list',
            'onclick': function () {
                document.execCommand('insertOrderedList', false, '');
                return true;
            }
        },
        {
            // Hyperlink button
            'type': 'icon',
            'icon': 'link',
            'name': 'link',
            'id': 'link',
            'title': 'Link',
            'onclick': function () {
                let selectedtext = window.getSelection().toString();
                let linktext = window.prompt("Please enter the link URL", selectedtext);
                document.execCommand('createLink', false, linktext);
                return true;
            }
        },
        {
            // Font drop down
            'type': 'drop',
            'id': 'font-faces',
            'values': [{
                    'name': 'Fonts',
                    'value': ''
                },
                {
                    'name': 'Arial',
                    'value': 'Arial'
                },
                {
                    'name': 'Arial Black',
                    'value': 'Arial Black'
                },
                {
                    'name': 'Courier',
                    'value': 'Courier'
                },
                {
                    'name': 'Courier New',
                    'value': 'Courier New'
                },
                {
                    'name': 'Georgia',
                    'value': 'Georgia'
                },
                {
                    'name': 'Helvetica',
                    'value': 'Helvetica'
                },
                {
                    'name': 'Times',
                    'value': 'Times'
                },
                {
                    'name': 'Trebuchet MS',
                    'value': 'Trebuchet MS'
                },
                {
                    'name': 'Verdana',
                    'value': 'Verdana'
                },
            ],
            'onchange': function () {
                document.execCommand('fontName', false, this.value);
                return true;
            }
        },
        {
            // Font size drop down
            'type': 'drop',
            'id': 'font-size',
            'values': [{
                    'name': 'Font sizes',
                    'value': ''
                },
                {
                    'name': 'Font size 1',
                    'value': '1'
                },
                {
                    'name': 'Font size 2',
                    'value': '2'
                },
                {
                    'name': 'Font size 3',
                    'value': '3'
                },
                {
                    'name': 'Font size 4',
                    'value': '4'
                },
                {
                    'name': 'Font size 5',
                    'value': '5'
                },
                {
                    'name': 'Font size 6',
                    'value': '6'
                },
                {
                    'name': 'Font size 7',
                    'value': '7'
                },
            ],
            'onchange': function () {
                document.execCommand('fontSize', false, this.value);
                return true;
            }
        },

        {
            // Heading drop down
            'type': 'drop',
            'id': 'header',
            'values': [{
                    'name': 'Headings',
                    'value': ''
                },
                {
                    'name': 'Heading 1',
                    'value': 'H1'
                },
                {
                    'name': 'Heading 2',
                    'value': 'H2'
                },
                {
                    'name': 'Heading 3',
                    'value': 'H3'
                },
                {
                    'name': 'Heading 4',
                    'value': 'H4'
                },
                {
                    'name': 'Heading 5',
                    'value': 'H5'
                },
                {
                    'name': 'Heading 6',
                    'value': 'H6'
                },
            ],
            'onchange': function () {
                document.execCommand('heading', false, this.value);
                return true;
            }
        }

    ];
    var $inBtns = $('#input-controls');
    $inBtns.html('');

    // Loop through and build buttons and drop downs
    buttonList.forEach(item => {
        switch (item.type) {
            // Build button
            case 'icon':
                var $btn = $('<button>');
                var $i = $('<i>');
                $i.addClass('fas');
                $i.addClass('fa-' + item.icon);
                $i.attr('id', 'in-icon-' + item.id);
                $btn.attr('id', 'in-btn-' + item.id);
                $btn.attr('title', item.title);
                $btn.append($i);
                $btn.on('click', item.onclick);
                $inBtns.append($btn);
                break;
            // Build drop down
            case 'drop':
                var $select = $('<select>');
                $select.attr('id', 'in-sel-' + item.id);
                $select.on('input propertychange', item.onchange);
                item.values.forEach(option => {
                    var $option = $('<option>');
                    $option.text(option.name);
                    $option.attr('value', option.value);
                    $select.append($option);
                });
                $inBtns.append($select);
                break;
        }
    });
});